import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:space_mission/bloc/search/search_bloc.dart';

class Search extends StatelessWidget {
  final TextEditingController textEditingController;
  const Search({required this.textEditingController});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
      padding: EdgeInsets.only(right: 10),
      height: 45,
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.all(Radius.circular(6.0))),
      child: TextField(
          decoration: InputDecoration(
              hintText: 'Find space missions',
              border: InputBorder.none,
              prefixIcon: Icon(Icons.search),
              suffix: GestureDetector(
                onTap: () {
                  textEditingController.text = '';
                  BlocProvider.of<SearchBloc>(context).add(ClearEvent());
                },
                child: Text('Clear'),
              )),
          controller: textEditingController),
    );
  }
}
