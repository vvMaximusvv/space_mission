import 'package:flutter/material.dart';
import 'package:space_mission/model/mission.dart';

class CustomListView extends StatelessWidget {
  final ScrollController scrollController;
  final List<Mission> missions;
  final bool isLoading;
  const CustomListView(
      {required this.scrollController, required this.missions, required this.isLoading});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: CustomScrollView(
            controller: scrollController,
            slivers: [
              SliverList(
                delegate: SliverChildBuilderDelegate(
                  (context, index) {
                    final Mission mission = missions[index];
                    return Card(
                      color: Colors.grey[200],
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Text(
                                'Space mission:',
                                style: TextStyle(color: Colors.black54, fontSize: 16),
                              ),
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Text(mission.missionName),
                                ),
                              ),
                            ],
                          ),
                          mission.details.isNotEmpty
                              ? Container(
                                  decoration: BoxDecoration(
                                      color: Colors.black12,
                                      border: Border.all(color: Colors.grey),
                                      borderRadius: BorderRadius.all(Radius.circular(6.0))),
                                  margin: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                                  child: Text(mission.details),
                                )
                              : Container()
                        ],
                      ),
                    );
                  },
                  childCount: missions.length,
                ),
              ),
            ],
          ),
        ),
        isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Container()
      ],
    );
  }
}
