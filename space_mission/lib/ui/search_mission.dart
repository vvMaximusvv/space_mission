import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/search/search_bloc.dart';
import 'widgets/list_view.dart';
import 'widgets/search.dart';

class SearchMission extends StatefulWidget {
  const SearchMission({Key? key}) : super(key: key);

  @override
  _SearchMissionState createState() => _SearchMissionState();
}

class _SearchMissionState extends State<SearchMission> {
  ScrollController scrollController = ScrollController();
  TextEditingController textEditingController = TextEditingController();
  bool isLoading = false;

  @override
  void initState() {
    textEditingController.addListener(() {
      if (textEditingController.text.length > 3) {
        BlocProvider.of<SearchBloc>(context).add(SearchMissions(textEditingController.text));
      }
    });
    scrollController.addListener(() {
      if (scrollController.position.pixels > 0.9 * scrollController.position.maxScrollExtent) {
        if (textEditingController.text.length > 3 && !isLoading) {
          BlocProvider.of<SearchBloc>(context).add(SearchMissions(textEditingController.text));
        }
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    textEditingController.dispose();
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Search(textEditingController: textEditingController),
            Expanded(
              child: BlocBuilder<SearchBloc, SearchState>(
                builder: (context, state) {
                  if (state is GetMissionsState) {
                    isLoading = state.isMissionsLoading;
                    return CustomListView(
                        scrollController: scrollController,
                        missions: state.missions,
                        isLoading: isLoading);
                  } else {
                    return Container();
                  }
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
