import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:space_mission/model/mission.dart';
import 'package:space_mission/services/graph_service.dart';

import 'search_repository.dart';

class SearchRepositoryImp extends SearchRepository {
  @override
  Future<Missions> fetchMissions(String query, int offset) async {
    QueryResult queryResult = (await GraphQLService().performQuery(query, offset));
    if (queryResult.data != null) {
      return Missions.fromJson(queryResult.data as Map<String, dynamic>);
    } else {
      return Missions(launchesPast: [], typeName: '');
    }
  }
}
