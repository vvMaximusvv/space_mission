import 'package:space_mission/model/mission.dart';

abstract class SearchRepository {
  Future<Missions> fetchMissions(String query, int offset);
}
