import 'package:graphql/client.dart';

class GraphQLService {
  GraphQLClient _client = GraphQLClient(link: HttpLink(''), cache: GraphQLCache());

  GraphQLService() {
    HttpLink link = HttpLink('https://api.spacex.land/graphql/');

    _client = GraphQLClient(link: link, cache: GraphQLCache());
  }

  Future<QueryResult> performQuery(String query, int offset) async {
    QueryOptions options = QueryOptions(
        document: gql(
          '''
  {
    launchesPast(limit: 10, find: {mission_name: "$query"}, offset: $offset) {
        mission_name
        details
    }
  }
''',
        ),
        variables: {});

    final result = await _client.query(options);

    return result;
  }
}
