class SpaceData {
  Missions missionsData;

  SpaceData({required this.missionsData});

  factory SpaceData.fromJson(Map<String, dynamic> parsedJson) {
    return SpaceData(missionsData: parsedJson['data']);
  }
}

class Missions {
  final String typeName;
  final List<Mission> launchesPast;
  const Missions({required this.typeName, required this.launchesPast});

  factory Missions.fromJson(Map<String, dynamic> parsedJson) {
    var list = parsedJson['launchesPast'] as List;
    List<Mission> launchesPast = list.map((i) => Mission.fromJson(i)).toList();

    return Missions(typeName: parsedJson['__typename'] ?? '', launchesPast: launchesPast);
  }
}

class Mission {
  String missionName;
  String details;

  Mission({required this.missionName, required this.details});

  factory Mission.fromJson(Map<String, dynamic> parsedJson) {
    return Mission(
        missionName: parsedJson['mission_name'] ?? '', details: parsedJson['details'] ?? '');
  }
}
