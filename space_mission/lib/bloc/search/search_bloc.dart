import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:space_mission/model/mission.dart';
import 'package:space_mission/repository/search_repository.dart';
import 'package:space_mission/repository/search_repository_imp.dart';
import 'package:space_mission/services/graph_service.dart';

part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  SearchBloc() : super(SearchInitial());
  SearchRepository searchRepositoryImp = SearchRepositoryImp();
  GraphQLService service = GraphQLService();

  List<Mission> launchesPast = [];

  @override
  Stream<SearchState> mapEventToState(
    SearchEvent event,
  ) async* {
    if (event is SearchMissions) {
      yield GetMissionsState(launchesPast, true);
      Missions missions = await searchRepositoryImp.fetchMissions(
          event.query, launchesPast.isEmpty ? 0 : launchesPast.length);
      launchesPast.addAll(missions.launchesPast);
      yield GetMissionsState(launchesPast, false);
    } else if (event is ClearEvent) {
      launchesPast = [];
      yield GetMissionsState(launchesPast, false);
    }
  }
}
