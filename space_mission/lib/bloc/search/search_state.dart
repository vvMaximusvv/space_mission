part of 'search_bloc.dart';

@immutable
abstract class SearchState extends Equatable {
  const SearchState();
}

class SearchInitial extends SearchState {
  @override
  List<Object> get props => [];
}

class GetMissionsState extends SearchState {
  const GetMissionsState(this.missions, this.isMissionsLoading);

  final List<Mission> missions;
  final bool isMissionsLoading;

  @override
  List<Object> get props => [missions, isMissionsLoading];
}

class MissionLoading extends SearchState {
  @override
  List<Object> get props => [];
}
