part of 'search_bloc.dart';

@immutable
abstract class SearchEvent extends Equatable {
  const SearchEvent();
}

// ignore: must_be_immutable
class SearchMissions extends SearchEvent {
  SearchMissions(this.query);
  String query;

  @override
  List<Object> get props => [query];
}

class ClearEvent extends SearchEvent {
  @override
  List<Object> get props => [];
}
