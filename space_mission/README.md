# space_mission

A test mobile app.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

1. To work with Flutter app, first of all you need to [install Flutter SDK](https://flutter.dev/docs/get-started/install)
2. Install the latest stable version of [Xcode](https://developer.apple.com/xcode/)
3. Configure the Xcode command-line tools to use the newly-installed version of Xcode by running the following from the command line:
```
sudo xcode-select --switch /Applications/Xcode.app/Contents/Developer
sudo xcodebuild -runFirstLaunch
```
4. Run `flutter doctor` to check that everithing works fine.
5. [Install Git](https://git-scm.com/) on your machine


## Running the tests

Run all tests in app

```
flutter test
```

## Dependencies

### Project libs
| flutter_bloc: ^7.0.1 | A Flutter package that helps implement the BLoC 
| equatable: ^2.0.3 | Being able to compare objects in Dart often involves having to override the == operator as well as hashCode.
| graphql_flutter: ^5.0.0 | provides an idiomatic flutter API and widgets for graphql/client.dart. 
| bloc_test | A Dart package that makes testing blocs and cubits easy. Built to work with bloc and mocktail.

## Author

* **Maxim Zaika**