import 'package:bloc_test/bloc_test.dart';
import 'package:space_mission/bloc/search/search_bloc.dart';
import 'package:space_mission/model/mission.dart';
import 'package:test/test.dart';
import 'package:mocktail/mocktail.dart';

class MockSearchBloc extends MockBloc<SearchEvent, SearchState> implements SearchBloc {}

List<Mission> missions = [Mission(missionName: 'Crew', details: 'Crew mission')];

void main() {

  setUpAll(() {
    registerFallbackValue<SearchEvent>(SearchMissions('Star'));
    registerFallbackValue<SearchState>(GetMissionsState(missions, true));
  });

  mainBloc();
}

void mainBloc() {
  group('whenListen', () {
    test("Let's mock the SearchBloc's stream!", () {
      final bloc = MockSearchBloc();

      whenListen(bloc, Stream<SearchState>.value(GetMissionsState(missions, true)));

      expectLater(bloc.stream, emitsInOrder([GetMissionsState(missions, true)]));
    });
  });

  group('SearchBloc', () {
    blocTest<SearchBloc, SearchState>('When there is no value in the database ',
        build: () => SearchBloc(),
        act: (bloc) => bloc.add(SearchMissions('Crew122')),
        expect: () => [
              GetMissionsState([], true),
              GetMissionsState([], false),
            ]);

    blocTest<SearchBloc, SearchState>('When clear caсhe data',
        build: () => SearchBloc(),
        act: (bloc) => bloc.add(ClearEvent()),
        expect: () => [GetMissionsState([], false)]);
  });
}
